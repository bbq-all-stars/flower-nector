var app = new Framework7({
  root: '#app',
  name: 'はなみつ',
  id: 'com.flower-nector',
  panel: {
    swipe: 'left',
  },
  routes: [
    {
      name: 'top',
      path: '/',
      url: '../index.html',
    },
    {
      name: 'result',
      path: '/result/',
      url: './result.html',
      on: {
        pageAfterIn: function (e, page) {
          showResult();
        }
      }
    },
    {
      name: 'map',
      path: '/map/.',
      url: '../map.html',
    }
  ],
});

app.views.create('.view-main');

var appSetting = {
  "flowers" : {
    "salvia splendens":{
      "poisonous_quantity": "little",
        "zh": {
        "name": "一串红",
          "reference": "https://zh.wikipedia.org/zh-tw/%E4%B8%80%E4%B8%B2%E7%BA%A2"
      },
      "ja": {
        "name": "サルビア",
          "reference": "https://ja.wikipedia.org/wiki/%E3%82%B5%E3%83%AB%E3%83%93%E3%82%A2"
      }
    },
    "azalea" :{
      "poisonous_quantity": "none",
        "zh": {
        "name": "皋月杜鵑",
          "reference": "https://zh.wikipedia.org/zh-tw/%E7%9A%8B%E6%9C%88%E6%9D%9C%E9%B9%83"
      },
      "ja": {
        "name": "サツキ",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%82%B5%E3%83%84%E3%82%AD"
      }
    },
    "rhododendron":{
      "poisonous_quantity": "none",
        "warning" : {
        "reference_link": "http://kininarurabbit.jp/669.html"
      },
      "zh": {
        "name": "杜鵑花",
          "reference": "https://zh.wikipedia.org/zh-tw/%E6%9D%9C%E9%B5%91%E8%8A%B1%E5%B1%AC"
      },
      "ja": {
        "name": "ツツジ",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%83%84%E3%83%84%E3%82%B8"
      }
    },
    "trifolium pratensis" :{
      "poisonous_quantity": "none",
        "zh": {
        "name": "紅菽草",
          "reference": "https://zh.wikipedia.org/zh-tw/%E7%B4%85%E8%8F%BD%E8%8D%89"
      },
      "ja": {
        "name": "ムラサキツメクサ",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%83%A0%E3%83%A9%E3%82%B5%E3%82%AD%E3%83%84%E3%83%A1%E3%82%AF%E3%82%B5"
      }
    },
    "honeysuckle" :{
      "poisonous_quantity": "none",
        "zh": {
        "name": "忍冬(金銀花)",
          "reference": "https://zh.wikipedia.org/zh-tw/%E5%BF%8D%E5%86%AC"
      },
      "ja": {
        "name": "スイカズラ",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%82%B9%E3%82%A4%E3%82%AB%E3%82%BA%E3%83%A9"
      }
    },
    "henbit" :{
      "poisonous_quantity": "none",
        "zh": {
        "name": "寶蓋草",
          "reference": "https://zh.wikipedia.org/zh-tw/%E5%AE%9D%E7%9B%96%E8%8D%89"
      },
      "ja": {
        "name": "ホトケノザ",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%83%9B%E3%83%88%E3%82%B1%E3%83%8E%E3%82%B6"
      }
    },
    "four o clock flower" :{
      "poisonous_quantity": "none",
        "zh": {
        "name": "紫茉莉",
          "reference": "https://zh.wikipedia.org/zh-tw/%E7%B4%AB%E8%8C%89%E8%8E%89"
      },
      "ja": {
        "name": "オシロイバナ",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%82%AA%E3%82%B7%E3%83%AD%E3%82%A4%E3%83%90%E3%83%8A"
      }
    },
    "astragalus sinicus" :{
      "poisonous_quantity": "none",
        "zh": {
        "name": "紫雲英(紅花草)",
          "reference": "https://zh.wikipedia.org/zh-tw/%E7%B4%AB%E4%BA%91%E8%8B%B1"
      },
      "ja": {
        "name": "ゲンゲ（レンゲソウ）",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%82%B2%E3%83%B3%E3%82%B2"
      }
    },
    "japanese camellia" :{
      "poisonous_quantity": "none",
        "zh": {
        "name": "山茶花",
          "reference": "https://zh.wikipedia.org/zh-tw/%E5%B1%B1%E8%8C%B6%E8%8A%B1"
      },
      "ja": {
        "name": "ヤブツバキ",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%83%84%E3%83%90%E3%82%AD"
      }
    },
    "chinese hibiscus" :{
      "poisonous_quantity": "none",
        "zh": {
        "name": "朱槿",
          "reference": "https://zh.wikipedia.org/zh-tw/%E6%9C%B1%E6%A7%BF"
      },
      "ja": {
        "name": "ブッソウゲ",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%83%96%E3%83%83%E3%82%BD%E3%82%A6%E3%82%B2"
      }
    },
    "chinese ixora" :{
      "poisonous_quantity": "none",
        "zh": {
        "name": "仙丹花",
          "reference": "https://zh.wikipedia.org/zh-tw/%E4%BB%99%E4%B8%B9%E8%8A%B1"
      },
      "ja": {
        "name": "サンタンカ",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%82%B5%E3%83%B3%E3%82%BF%E3%83%B3%E3%82%AB"
      }
    },
    "impatiens balsamina" :{
      "poisonous_quantity": "none",
        "zh": {
        "name": "鳳仙花",
          "reference": "https://zh.wikipedia.org/zh-tw/%E5%87%A4%E4%BB%99%E8%8A%B1"
      },
      "ja": {
        "name": "ホウセンカ",
          "reference_link": "https://ja.wikipedia.org/wiki/%E3%83%9B%E3%82%A6%E3%82%BB%E3%83%B3%E3%82%AB"
      }
    },
  },
  "settings": {}
};
